#include <Arduino.h>
#include <WiFi.h>
#include <EEPROM.h>
#include <WiFiClient.h>
#include <WebServer.h>  
#include <FirebaseESP32.h>                             /*Esta libreria Admite la creación de un servidor web y la ejecución de un servidor web simple independiente.*/


//-------------------------------------VARIABLES y MACROS-----------------------------------------------------------------------------
#define BUTTONPIN 33                                                                             /*CREAMOS LAS MACROS DE LOS PINES 33 (ANALOGICO) Y 18 (DIGTAL)*/                                                                                                
#define BUTTONPIN2 18                                                                                                 
#define API_KEY "Argjxr74C4z2cfltdCms0zSNJayO9JV9gGrCexeV"                                        /*DEFINIMOS LA APIKEY DE LA BASE DE DATOS QUE UTILIZAREMOS*/
#define DATABASE_URL "https://proyectotawata-default-rtdb.firebaseio.com/"                         /*DEFINIMOS LA DATABASE_URL DE LA BASE DE DATOS QUE UTILIZAREMOS*/
#define WIFI 19
                                                                    
char ssid[50];                                                                                      /*Definimos dos variables tipo char llamadas ssid y pass*/
char pass[50];

WiFiClient espClient;
WebServer server(80);                                                                             /*Abriendo un puerto 80 para acceder al servidor web.*/


int valor = 0;
float voltaje;
bool flag = false;
int octopus; 



FirebaseData fbdo;
FirebaseAuth auth;                                                                                      /*Objetos de firebase a utilizar*/
FirebaseConfig config;


String leer(int addr);                                                                                /*Definimos los prototipos de las funciones leer y enviarindex*/
void enviarIndex();

//--------------------------------------------------------------------------------------------------------------------------

//------------------------------------ENCENDER LASER--------------------------------------------------------------------------
void ENCENDER_LASER()                                                                                               /*Esta funcion se utiliza para encender el laser*/
{
  bool dato;
  int laser1;
  dato = Firebase.getInt(fbdo, "/laser1");                                                                          /*El dato indica si el resultado de la operacion fue exitoso o no, si devulve un 1 fue exitoso y si devuelve un 0 no fue exitoso*/
  laser1 = fbdo.intData();                                                                                          /*Esa funcion nos trae la carga util de laser_1*/
  Serial.println(dato);
  Serial.println(laser1);
  digitalWrite(BUTTONPIN2, laser1);
}
//-----------------------------------------------------------------------------------------------------------------------
//------------------------------------CONECTAR WIFI--------------------------------------------------------------------------
void setup_wifi() {
    pinMode(WIFI, INPUT);
    octopus = digitalRead(WIFI);
    Serial.println(octopus);
    delay(5000);
    if(octopus == 1){
        WiFi.mode(WIFI_AP);
        WiFi.softAP("OCTOPUS", "");
        Serial.println("red OCTOPUS abierta");

    }else{
 
    WiFi.mode(WIFI_STA);
    leer(0).toCharArray(ssid, 50);
    leer(50).toCharArray(pass, 50);
    WiFi.begin(ssid, pass);                                                                         /*Otorgamos la ssid y la pass de la red que queremos conectar*/
    int contconexion = 0;
    while (WiFi.status() != WL_CONNECTED && contconexion < 30) {                                    /*El contador cuenta hasta 30 egundos si no se puede conectar lo cancela*/ 
        ++contconexion;
        delay(1000);
        Serial.print("-");
    }

    if (contconexion < 30) {
        Serial.println("");
        Serial.println("WiFi conectado");
        flag = true;
        Firebase.begin(DATABASE_URL, API_KEY);
        Firebase.reconnectWiFi(true);
    }
    else {
        Serial.println("");
        Serial.println("Error de conexion");
    }

    
    }
    
        
    
}

//----------------------------------------------------------------------------------------------------------------------

//-----------------------------------------GRABAR LA EEPROM-------------------------------------------------------------
void grabar(int addr, String a) {                                                                   /*Esta funcion grabar recibe 2 parametros el entero addr que es la direccion de la eeprom (0 y 50)*/
    int tamano = a.length();                                                                        /*define el tamaño del string*/
    char inchar[50];                                                                                /*define un string de 50 caracteres*/
    a.toCharArray(inchar, tamano + 1);
    for (int i = 0; i < tamano; i++) {                                                              /*Escribe la EEPROM celda por celda*/
        EEPROM.write(addr + i, inchar[i]);
    }
    for (int i = tamano; i < 50; i++) {
        EEPROM.write(addr + i, 255);
    }
    EEPROM.commit();
    Serial.println("EEPROM grabada");
}

//---------------------------------------------------------------------------------------------------------------

//---------------------------------------LEER LA EEPROM----------------------------------------------------------
String leer(int addr) {
    byte lectura;
    String strlectura;                                                                              /*Lee celda por celda de la EEPROM*/
    for (int i = addr; i < addr + 50; i++) {
        lectura = EEPROM.read(i);
        if (lectura != 255) {
            strlectura += (char)lectura;
        }
    }
    Serial.println("lectura de EEPROM");
    Serial.println(ssid);
    Serial.println(pass);
    return strlectura;
}
//---------------------------------------------------------------------------------------------------------------

//----------------------------------GRABADO DE LA EEPROM---------------------------------------------------------
void guardar_conf() {
    Serial.println("Red:");
    Serial.println(server.arg("ssid")); 
    grabar(0, server.arg("ssid"));                                                                  /*Recibimos los valor de ssid que inserto el usurio en la pagina html, gracias al fetch y lo graba en la poscion 0*/ 
    Serial.println(server.arg("pass"));  
    grabar(50, server.arg("pass"));                                                                 /*Recibimos los valor de pass que inserto el usurio en la pagina html, gracias al fetch y lo graba en la posicion 50*/

    server.sendHeader("Connection", "close");
    server.send(200, "text/html", "OK");

    ESP.restart();
}

//------------------------------------------------------------------------------------------------------------------

void setup() {
    EEPROM.begin(512);                                                                                /*inicalizamos la EEPROM para tener una capacidad de 512 bits*/
    Serial.begin(9600);
    pinMode(BUTTONPIN, INPUT);
    pinMode(BUTTONPIN2, OUTPUT);                                                                      /*DEFINIMOS EL PIN 33 COMO SALIDA Y EL PIN 18 COMO ENTRADA*/
    setup_wifi();                                                                                     /*Llama a la funcion setup_wifi*/
    server.begin();                                                                                    /*Indica al servidor que comience a escuchar las conexiones entrantes.*/
    server.on("/", HTTP_GET, enviarIndex);                                                              /*La función server.on() tiene 2 parámetros, la primera parte es el punto final que espera desde el lado del cliente. y la 2ª parte es una función que se ejecuta al activar el punto final. La función se define en el mismo archivo de código.*/
    server.on("/s", guardar_conf);
    
    

}

void loop() {
    server.handleClient();                                                                                           /*Manejador de cliente .Espera el llamado del usuario*/
    delay(1);
 //   ENCENDER_LASER();                                                                                                       /*ESTA FUNCION LA ESTARA LLAMANDO CONSTANTEMENTE*/
    if(flag == true){
        ENCENDER_LASER();                                                                                                       /*ESTA FUNCION LA ESTARA LLAMANDO CONSTANTEMENTE*/
        Serial.println("valor");                                                                      
        valor = analogRead(BUTTONPIN);                                                                                     /*LEERA EL PIN 33 Y LO GUARDARA EN LA VARIABLE valor*/
        voltaje = ((valor * 3.3) / 4096);                                                                                  /*REALIZA EL CALCULO MATEMATICO*/
        Serial.println(voltaje);
        delay(5000);
        Firebase.setFloat(fbdo, "/dato", voltaje);                                                                         /*ENVIA LA VARIABLE VOLTAJE A LA BASE DE DATOS*/
    }
}

void enviarIndex() {                                                                                                        /*Funcion llamada enviarindex, dentro de esta vemos el codigo html que guardara el esp32,dentro del string*/
    String index = ""                        
                   "<!DOCTYPE html><html lang=\"en\">"
                   "<head><meta charset=\"UTF-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><title>Wifi</title>"
                   "</head>"
                   "<body>"
                   "<div class=\"mb-2\">"
                   "<label for=\"exampleInputEmail1\" class=\"form-label\">SSID</label>"
                   "<input type=\"email\" class=\"form-control\" id=\"SSIDId\">"
                   "</div>"
                   "<div class=\"mb-2\">"
                   "<label for=\"exampleInputEmail1\" class=\"form-label\">Password</label>"
                   "<input type=\"email\" class=\"form-control\" id=\"PasswordId\">"
                   "</div>"
                   "<div class=\"col-lg-7 col-md-8 col-sm-8 col-12\">"
                   "<button type=\"button\" style=\"width: 100%; margin-top: 1rem;\" class=\"btn btn-personal\""
                   "id=\"conectarseId\">Conectarse</button>"
                   "</div> "
                   "<table><tbody>";
    int wifiCount = WiFi.scanNetworks();                                                                                    /*Esta fucnion escanea las redes wifi del entorno y las guarda en wificount */
    for (size_t i = 0; i < wifiCount; i++)                                                                                  /*Este for lo que hace es tomar todas de las redes escaneadas y concatenarlas*/
    {
        String nombre = WiFi.SSID(i);
        index.concat("<tr>");
        index.concat("<td>");
        index.concat(nombre);
        index.concat("<td>");
        index.concat("</tr>");
    }
    String index_2 = ""
                     "</tbody></table>"
                     "<script>"
                     "var SSIDRef = document.getElementById(\"SSIDId\");"
                     "var passwordRef = document.getElementById(\"PasswordId\");"
                     "var conectarseRef = document.getElementById(\"conectarseId\");"
                     "conectarseRef.addEventListener(\"click\", Enviardatos);"
                     " function Enviardatos() {"
                     "var ssid = SSIDRef.value;"
                     "var pass = passwordRef.value;"
                     "let fetchRes = fetch(\"http://192.168.4.1/s?ssid=\" + ssid + \"&pass=\" + pass);"
                     "}"
                     "</script>"
                     "</body>"
                     "</html>";
    index.concat(index_2);
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", index);
}
