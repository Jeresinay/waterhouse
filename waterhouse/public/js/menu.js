import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, onAuthStateChanged} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, push, get, remove, onValue, set} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDomD3BABlSmX75HI3--r8Idhgih2zEFW8",
  authDomain: "waterhouse-b4c24.firebaseapp.com",
  projectId: "waterhouse-b4c24",
  storageBucket: "waterhouse-b4c24.appspot.com",
  messagingSenderId: "928177087601",
  appId: "1:928177087601:web:198571ad764b05bc3da7d0"
};

//inicializo firebase 
const app = initializeApp(firebaseConfig);
const auth = getAuth();
const database = getDatabase(app);



console.log("Bienvenido a la consola de pruebas.");
// Serial.println("...");

let caudalREF = document.getElementById("caudal");

var registroRef = document.getElementById("registroid");
//console.log(registroRef);

var ingresoRef = document.getElementById("ingresoid");
//console.log(ingresoRef);

var logoutRef = document.getElementById("logoutid");
//console.log(logoutRef);

var EncenderRef = document.getElementById("EncenderId")

var ApagarRef = document.getElementById("ApagarId")

var cardRef = document.getElementById("cardinicioid");
//console.log(cardRef);



var UserID;
var UserUID;
var usuario;



logoutRef.addEventListener("click", LogOut);



onAuthStateChanged(auth, (user) => {

    // solo se puede ver los botones para registrar e ingresar
    logoutRef.style.display = "none";
    ingresoRef.style.display = "block";
    registroRef.style.display = "block";
    caudalREF.style.display = "none";
    ApagarRef.style.display = "none";
    EncenderRef.style.display = "none";
    
    
  
    if (user) {
        usuario = user
        // una vez iniciado sesión, se puede ver el botón para cerrar sesión 
        logoutRef.style.display = "block";
        ingresoRef.style.display = "none";
        registroRef.style.display = "none";
        caudalREF.style.display = "block";
        ApagarRef.style.display = "block";
        EncenderRef.style.display = "block";
         
        get(ref(database, "users/" + user.uid)).then((snap) => {
          usuario = snap.val();
          UserID = usuario.Name;
          UserUID = user.uid;
        })
    }
});


function LogOut(){
  auth.signOut();
  location.reload(true);
}




const rutaRef = ref(database, 'dato/');
onValue(rutaRef,(snapshot) => {
  const data = snapshot.val();
  caudalREF.innerHTML = data;
  
  
})

EncenderRef.addEventListener("click",encender);

const valor1 = 1;

function encender(){
  const db = getDatabase();
  set(ref(db, "sensor1" ), valor1).then(() => {
    
})}

ApagarRef.addEventListener("click",apagar);

const valor0 = 0;

function apagar(){
  const db = getDatabase();
  set(ref(db, "sensor1" ), valor0).then(() => {
    
})}


